<?php

declare(strict_types=1);

namespace WEBprofil;

use TYPO3\CMS\Core\Cache\Backend\NullBackend;

/**
 * Class to use in your configuration files of a TYPO3 project.
 */
class Config extends \B13\Config
{
    public static function initialize(bool $applyDefaults = true): self
    {
        return parent::initialize()
            ->appendContextToSiteName()
            ->includeContextDependentConfigurationFiles();
    }

    public function useDDEVConfiguration(string $dbHost = null): self
    {
        parent::useDDEVConfiguration($dbHost);

        return self::$instance
            ->disableCaching()
            ->setFilefillConfiguration();
    }

    /**
     * @return $this
     */
    public function disableCaching(): self
    {
        foreach ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'] as $cacheName => $cacheConfiguration) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$cacheName]['backend'] = NullBackend::class;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setFilefillConfiguration(): self
    {
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['filefill'] = [
            'storages' => [
                1 => [
                    2 => [
                        'identifier' => 'sys_domain',
                    ],
                    3 => [
                        'identifier' => 'placeholder',
                    ],
                ]
            ]
        ];

        return $this;
    }
}
